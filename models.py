import datetime as _dt
from operator import index

import sqlalchemy as _sql
import sqlalchemy.orm as _orm
import passlib.hash as _hash

import database as _database


class User(_database.Base):
    __tablename__ = "users"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    email = _sql.Column(_sql.String, unique=True, index=True)
    hashed_password = _sql.Column(_sql.String)

    leads = _orm.relationship("Lead", back_populates="owner")

    def verify_password(self, password: str):
        return _hash.bcrypt.verify(password, self.hashed_password)


class Unit(_database.Base):
    __tablename__ = "unit"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    subbidang = _sql.Column(_sql.String, index=True)

    pegawai = _orm.relationship("Pegawai", back_populates="unit")
    kategori = _orm.relationship("Kategori", back_populates="unit")



class Pegawai(_database.Base):
    __tablename__ = "pegawai"
    nip = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_unit = _sql.Column(_sql.Integer, _sql.ForeignKey("unit.id"))
    nama_lengkap = _sql.Column(_sql.String, index=True)
    hashed_password = _sql.Column(_sql.String)

    unit = _orm.relationship("Unit", back_populates="pegawai")
    task_pegawai = _orm.relationship("Task_Pegawai", back_populates="pegawai")

    def verify_password(self, password: str):
        return _hash.bcrypt.verify(password, self.hashed_password)


class Kategori(_database.Base):
    __tablename__ = "kategori"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_unit = _sql.Column(_sql.Integer, _sql.ForeignKey("unit.id"))
    nama_kategori = _sql.Column(_sql.String, index=True)

    unit = _orm.relationship("Unit", back_populates="kategori")
    task = _orm.relationship("Task", back_populates="kategori")


class Task_Pegawai(_database.Base):
    __tablename__ = "task_pegawai"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_pegawai  = _sql.Column(_sql.Integer, _sql.ForeignKey("pegawai.nip"))
    id_task  = _sql.Column(_sql.Integer, _sql.ForeignKey("task.id"))
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
    date_last_updated = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)

    pegawai = _orm.relationship("Pegawai", back_populates="task_pegawai")
    task = _orm.relationship("Task", back_populates="task_pegawai")


class Task(_database.Base):
    __tablename__ = "task"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_kategori = _sql.Column(_sql.Integer, _sql.ForeignKey("kategori.id"))
    nama_task = _sql.Column(_sql.String, index=True, default="")
    deskripsi = _sql.Column(_sql.String, index=True, default="")
    deadline = _sql.Column(_sql.DATETIME, index=True, default="")
    risk = _sql.Column(_sql.String, index=True, default="")
    status = _sql.Column(_sql.String, nullable=False)

    kategori = _orm.relationship("Kategori", back_populates="task")
    todolist = _orm.relationship("ToDoList", back_populates="task")
    attachment = _orm.relationship("Attachment", back_populates="task")
    task_pegawai = _orm.relationship("Task_Pegawai", back_populates="task")


class ToDoList(_database.Base):
    __tablename__ = "todolist"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_task = _sql.Column(_sql.Integer, _sql.ForeignKey("task.id"))
    nama_todolist = _sql.Column(_sql.String, index= True)
    status = _sql.Column(_sql.Boolean, unique=_sql.false, default=True)
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
    date_last_updated = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)

    task = _orm.relationship("Task", back_populates="todolist")



class Attachment(_database.Base):
    __tablename__ = "attachment"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    id_task = _sql.Column(_sql.Integer, _sql.ForeignKey("task.id"))
    file_task = _sql.Column(_sql.String, index=True)
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
    date_last_updated = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)

    task = _orm.relationship("Task", back_populates="attachment")


class Lead(_database.Base):
    __tablename__ = "leads"
    id = _sql.Column(_sql.Integer, primary_key=True, index=True)
    owner_id = _sql.Column(_sql.Integer, _sql.ForeignKey("users.id"))
    first_name = _sql.Column(_sql.String, index=True)
    last_name = _sql.Column(_sql.String, index=True)
    email = _sql.Column(_sql.String, index=True)
    company = _sql.Column(_sql.String, index=True, default="")
    note = _sql.Column(_sql.String, default="")
    date_created = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)
    date_last_updated = _sql.Column(_sql.DateTime, default=_dt.datetime.utcnow)

    owner = _orm.relationship("User", back_populates="leads")
