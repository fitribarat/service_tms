import fastapi as _fastapi
import fastapi.security as _security
import jwt as _jwt
import datetime as _dt
import sqlalchemy.orm as _orm
import passlib.hash as _hash

import database as _database, models as _models, schemas as _schemas

oauth2schema = _security.OAuth2PasswordBearer(tokenUrl="/api/token")

JWT_SECRET = "myjwtsecret"


def create_database():
    return _database.Base.metadata.create_all(bind=_database.engine)


def get_db():
    db = _database.SessionLocal()
    try:
        yield db
    finally:
        db.close()


async def get_pegawai_by_nip(nip: int, db: _orm.Session):
    return db.query(_models.Pegawai).filter(_models.Pegawai.nip == nip).first()


async def authenticate_pegawai(nip: int, password: str, db: _orm.Session):
    pegawai = await get_pegawai_by_nip(db=db, nip=nip)

    if not pegawai:
        return False

    if not pegawai.verify_password(password):
        return False

    return pegawai


async def create_token(pegawai: _models.Pegawai):
    pegawai_obj = _schemas.Pegawai.from_orm(pegawai)

    token = _jwt.encode(pegawai_obj.dict(), JWT_SECRET)

    return dict(access_token=token, token_type="bearer")


async def get_current_pegawai(
    db: _orm.Session = _fastapi.Depends(get_db),
    token: str = _fastapi.Depends(oauth2schema),
):
    try:
        payload = _jwt.decode(token, JWT_SECRET, algorithms=["HS256"])
        pegawai = db.query(_models.Pegawai).get(payload["nip"])
    except:
        raise _fastapi.HTTPException(
            status_code=401, detail="Invalid Email or Password"
        )

    return _schemas.Pegawai.from_orm(pegawai)


async def create_unit(unit : _schemas.UnitCreate, db : _orm.Session):
    unit_obj = _models.Unit(subbidang = unit.subbidang)
    
    db.add(unit_obj)
    db.commit()
    db.refresh(unit_obj)
    return unit_obj


async def get_unit_by_id(id_unit: int, db: _orm.Session):
    return db.query(_models.Unit).filter(_models.Unit.id == id_unit).first()


async def create_kategori(kategori : _schemas.KategoriCreate, db : _orm.Session):
    kategori_obj = _models.Kategori(**kategori.dict())

    db.add(kategori_obj)
    db.commit()
    db.refresh(kategori_obj)
    return _schemas.Kategori.from_orm(kategori_obj)

async def get_kategori_by_id(id_kategori : int, db : _orm.Session):
    return db.query(_models.Kategori).filter(_models.Kategori.id == id_kategori).first()


async def create_pegawai(pegawai : _schemas.PegawaiCreate, db : _orm.Session):
    pegawai_obj = _models.Pegawai(nama_lengkap = pegawai.nama_lengkap, nip = pegawai.nip, id_unit = pegawai.id_unit, hashed_password = _hash.bcrypt.hash(pegawai.hashed_password))

    db.add(pegawai_obj)
    db.commit()
    db.refresh(pegawai_obj)
    return _schemas.Pegawai.from_orm(pegawai_obj)


async def create_task(task : _schemas.TaskCreate, db : _orm.Session ):
    task_obj = _models.Task(**task.dict())

    db.add(task_obj)
    db.commit()
    db.refresh(task_obj)
    return _schemas.Task.from_orm(task_obj)