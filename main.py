from typing import List
import fastapi as _fastapi
import fastapi.security as _security
import sqlalchemy.orm as _orm
import services as _services, schemas as _schemas

app = _fastapi.FastAPI()


@app.post("/api/token")
async def generate_token(
    form_data: _security.OAuth2PasswordRequestForm = _fastapi.Depends(),
    db: _orm.Session = _fastapi.Depends(_services.get_db),
):
    pegawai = await _services.authenticate_pegawai(form_data.username, form_data.password, db)

    if not pegawai:
        raise _fastapi.HTTPException(status_code=401, detail="Invalid Credentials")

    return await _services.create_token(pegawai)


@app.get("/api/pegawai/me", response_model=_schemas.Pegawai)
async def get_pegawai(pegawai: _schemas.Pegawai = _fastapi.Depends(_services.get_current_pegawai)):
    return pegawai

@app.post("/api/unit")
async def create_unit(
    unit : _schemas.UnitCreate, db : _orm.Session = _fastapi.Depends(_services.get_db)
):
    return await _services.create_unit(unit, db)


@app.get("/api/units/{id_unit}", status_code=200)
async def get_unit (
    id_unit : int,
    db : _orm.Session = _fastapi.Depends(_services.get_db)
):
    return await _services.get_unit_by_id(id_unit, db)


@app.post("/api/kategori")
async def create_kategori(
    kategori : _schemas.KategoriCreate, 
    db : _orm.Session = _fastapi.Depends(_services.get_db),
):
    db_unit = await _services.get_unit_by_id(kategori.id_unit , db)
    if not db_unit :
        raise _fastapi.HTTPException(status_code=400, detail="Unit tidak terdaftar")
    
    return await _services.create_kategori(kategori, db )


@app.get("/api/kategori/{id_kategori}", status_code=200)
async def get_kategori (
    id_kategori : int,
    db : _orm.Session = _fastapi.Depends(_services.get_db)
): 
    return await _services.get_kategori_by_id(id_kategori, db)
    

@app.post("/api/pegawai")
async def create_pegawai(
    pegawai : _schemas.PegawaiCreate,
    db : _orm.Session = _fastapi.Depends(_services.get_db)
):
    db_pegawai = await _services.get_pegawai_by_nip(pegawai.nip, db)
    db_unit = await _services.get_unit_by_id(pegawai.id_unit, db)

    if not db_unit :
        raise _fastapi.HTTPException(status_code=400, detail="Unit tidak terdaftar")

    elif db_pegawai :
            raise _fastapi.HTTPException(status_code=400, detail="Pegawai sudah terdaftar")

    pegawai = await _services.create_pegawai(pegawai, db)

    return await _services.create_token(pegawai)


@app.post("/api/task")
async def create_task(
    task : _schemas.TaskCreate,
    db : _orm.Session = _fastapi.Depends(_services.get_db)
):
    db_kategori = await _services.get_kategori_by_id(task.id_kategori, db)
    
    if not db_kategori :
        raise _fastapi.HTTPException(status_code=400, detail="Kategori tidak terdaftar")

    return await _services.create_task(task, db)


@app.get("/api")
async def root():
    return {"Message" : "Awesome System"}

