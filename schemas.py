import datetime as _dt

import pydantic as _pydantic


class _UserBase(_pydantic.BaseModel):
    email: str


class UserCreate(_UserBase):
    hashed_password: str

    class Config:
        orm_mode = True


class User(_UserBase):
    id: int

    class Config:
        orm_mode = True

# ====================================================

class _UnitBase(_pydantic.BaseModel):
    subbidang : str

class UnitCreate(_UnitBase):
    pass

class Unit(_UnitBase):
    id : int

    class Config:
        orm_mode = True

# =========================================================

class _PegawaiBase(_pydantic.BaseModel):
    nama_lengkap : str
    id_unit : int
    nip : int

class PegawaiCreate(_PegawaiBase):
    hashed_password: str

    class Config:
        orm_mode = True

class Pegawai(_PegawaiBase):
    nip : int 

    class Config:
        orm_mode = True

# ================================================================

class _KategoriBase(_pydantic.BaseModel):
    nama_kategori : str
    id_unit : int

class KategoriCreate(_KategoriBase):
    pass

class Kategori(_KategoriBase):
    id : int

    class Config:
        orm_mode = True

# =================================================================

class _TaskPegawaiBase(_pydantic.BaseModel):
    pass

class TaskPegawaiCreate(_TaskPegawaiBase):
    pass

class TaskPegawai(_TaskPegawaiBase):
    id : int
    id_pegawai : int
    id_task : int
    data_created : _dt.datetime
    date_last_updated : _dt.datetime

    class Config:
        orm_mode = True

# ======================================================================

class _TaskBase(_pydantic.BaseModel):
    deskripsi : str
    risk : str
    id_kategori : int
    deadline : _dt.date
    nama_task : str
    status : bool

class TaskCreate(_TaskBase):
    pass

class Task(_TaskBase):
    id : int
    id_kategori : int
    

    class Config:
        orm_mode = True

# ======================================================================

class _TodolistBase(_pydantic.BaseModel):
    nama_todolist : str
    risk : str

class TodolistCreate(_TodolistBase):
    pass

class Task(_TodolistBase):
    id : int
    id_task : int
    status : str
    data_created : _dt.datetime
    date_last_updated : _dt.datetime

    class Config:
        orm_mode = True

# ======================================================================

class _AttachmentBase(_pydantic.BaseModel):
    file_task : str

class AttachmentCreate(_AttachmentBase):
    pass

class Task(_AttachmentBase):
    id : int
    id_task : int
    data_created : _dt.datetime
    date_last_updated : _dt.datetime

    class Config:
        orm_mode = True

# ======================================================================


class _LeadBase(_pydantic.BaseModel):
    first_name: str
    last_name: str
    email: str
    company: str
    note: str


class LeadCreate(_LeadBase):
    pass


class Lead(_LeadBase):
    id: int
    owner_id: int
    date_created: _dt.datetime
    date_last_updated: _dt.datetime

    class Config:
        orm_mode = True
